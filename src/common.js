exports.checkValid = function (data) {
	if (data == '' || data == null || data == undefined) {
		return false;
	}
	else {
		return true;
	}
};

exports.cleanJSON = function (data) {
	return (JSON.stringify(data, null, 4) + '\n');
};

//arrayDiff is used to subtract arrays, see usage example below.
//[1,2,3,4,5,6].arrayDiff( [3,4,5] ) will return [1, 2, 6]
//Credits goes to Joshaven Potter
//Original answer on stackoverflow : http://stackoverflow.com/a/4026828
exports.arrayDiff = function (arrayOfArrayElementsToRemove) {
	return this.filter(function (i) { return arrayOfArrayElementsToRemove.indexOf(i) < 0; });
};

exports.toLocalISOString = function (now) {
	var tzo = -now.getTimezoneOffset(),
		dif = tzo >= 0 ? '+' : '-',
		pad = function (num) {
			var norm = Math.abs(Math.floor(num));
			return (norm < 10 ? '0' : '') + norm;
		};
	return now.getFullYear()
		+ '-' + pad(now.getMonth() + 1)
		+ '-' + pad(now.getDate())
		+ 'T' + pad(now.getHours())
		+ ':' + pad(now.getMinutes())
		+ ':' + pad(now.getSeconds())
		+ dif + pad(tzo / 60)
		+ ':' + pad(tzo % 60);
};