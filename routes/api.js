//In case of a running the app for the first time or launch of a new IPO, do the following:
//1 Go to https://www.quandl.com/data/NSE and see if the new scrip is available, if it is available continue to Step 2
//2 Send a GET request to /api/datasets and check the console to see if the retrieval is complete
//3 Send a GET request to /api/dataForDatasets to download data for the new scrips (SAVES DATA)
//OPTIONAL : You can specify the oldest date to fetch using the in the 'oldestDate' variable in the 'App config' section below. All data from that date will be fetched and stored
//OPTIONAL : Specify your Quandl API key in the 'quandlAuthToken' variable in the 'App config' section below

//To update data for existing datasets, do the following
//1 Send a GET request to /api/updateDataForExistingDatasets
//OPTIONAL : You can specify the number of simultaneous updates to perform using the 'maxSimultaneousUpdates' variable in the 'App config' section below

//The following API routes available:
// GET /
// PUT /downloadDatasets
// PUT /downloadDataForDatasetsWithoutData
// POST /updateDataForExistingDatasets

//General
var express = require('express');
var router = express.Router();
var request = require('request');
var debug = require('debug')('http');
var jwt = require('jsonwebtoken');

//Error logger
// var path = require('path');
// var fs = require('fs');
// var writer = fs.createWriteStream(path.join(__dirname,'/../','/r2sd.log'));

//App config
var config = require('../config.js');
var quandlAuthToken = config.quandlAuthToken;
var oldestDate = config.oldestDate;
var maxSimultaneousUpdates = config.maxSimultaneousUpdates;//Quandl throws errors if the concurrent request count is 4 or more

//Mongoose and mongoDB
var NSEDatasets = require('../model/schema').NSEDatasetModel;
var DatasetContents = require('../model/schema').DatasetContentModel;
var DelistedDatasets = require('../model/schema').DelistedDatasetModel;
var common = require('../src/common.js');

var pseudoToday;

var serverSecret = config.serverSecret;

router.use(function (req, res, next) {
	var token = req.headers['x-access-token'] || req.query.token || req.body.token;
	jwt.verify(token, serverSecret, function (error, decoded) {
		if (error) {
			if (error.name === 'TokenExpiredError')
				res.status(401).json({
					success: false,
					message: 'Token expired, get a newer token by LOGGING IN AGAIN'
				});
			else
				res.status(401).json({
					success: false,
					message: 'Invalid Token, try getting a token by LOGGING IN AGAIN'
				});
			cleanJSON(error);
		}
		else {
			res.user = decoded;
			cleanJSON(res.user);
			next();
		}
	});
});

function cleanJSON(data) {
	console.log(common.cleanJSON(data));
}

function parseToObject(inputArray, outputArray) {
	var i;
	if (inputArray[0].length === 8)
		for (i = 0; i < inputArray.length; i++) {
			outputArray[i] = {
				'Date': inputArray[i][0],
				'Open': inputArray[i][1],
				'High': inputArray[i][2],
				'Low': inputArray[i][3],
				'Last': inputArray[i][4],
				'Close': inputArray[i][5],
				'Total Trade Quantity': inputArray[i][6],
				'Turnover (Lacs)': inputArray[i][7]
			};
		}
	else
		for (i = 0; i < inputArray.length; i++) {
			outputArray[i] = {
				'Date': inputArray[i][0],
				'Open': inputArray[i][1],
				'High': inputArray[i][2],
				'Low': inputArray[i][3],
				'Close': inputArray[i][4],
				'Shares Traded': inputArray[i][5],
				'Turnover (Rs. Cr)': inputArray[i][6]
			};
		}
}

function downloadDatasets(builtDatasets, page, callback) {
	var url = 'https://www.quandl.com/api/v3/datasets.json?database_code=NSE&per_page=100&sort_by=id&page=' + page + '&api_version=2015-04-09&auth_token=' + quandlAuthToken;
	try {
		request.get({ 'url': url, 'timeout': 20000 }, function (error, response) {
			if (error) {
				callback(error);
				downloadDatasets(builtDatasets, page, callback);
			}
			else {
				if (common.checkValid(response) && common.checkValid(response.body)) {
					var parsedResponse = JSON.parse(response.body);
					if (common.checkValid(parsedResponse) && common.checkValid(parsedResponse.meta) && common.checkValid(parsedResponse.meta.current_page)) {
						console.log('Downloaded page ' + parsedResponse.meta.current_page + '/' + parsedResponse.meta.total_pages);
						builtDatasets = builtDatasets.concat(parsedResponse.datasets);
						if (parsedResponse.meta.next_page)
							downloadDatasets(builtDatasets.slice(0), parsedResponse.meta.next_page, callback);//builtDatasets.slice(0) is essential as array response will be passed by default which has been causing data overwrite errors.
						else {
							callback(null, builtDatasets);
						}
					}
					else {
						downloadDatasets(builtDatasets.slice(0), page, callback);
					}
				}
				else {
					console.log('Retrying page ' + page);
				}
			}
		});
	} catch (error) {
		console.log('Failed to download page ' + page + ' due to the following exception');
		cleanJSON(error);
		console.log('Reattempting to download page ' + page);
		downloadDatasets(builtDatasets.slice(0), page, callback);
	}
}

function downloadAndStoreDatasetContents(data, iteration) {
	var url = 'https://www.quandl.com/api/v3/datasets/' + data[iteration].database_code + '/' + data[iteration].dataset_code + '/data.json?auth_token=' + quandlAuthToken + '&api_version=2015-04-09&start_date=' + oldestDate;
	try {
		request.get(url, function (error, response) {
			if (error) {
				cleanJSON(error);
				downloadAndStoreDatasetContents(data, iteration);
			}
			else {
				if (common.checkValid(response) && common.checkValid(response.body)) {
					var parsedResponse = JSON.parse(response.body);
					console.log('(' + (iteration + 1) + '/' + data.length + ') ' + 'Downloaded data for ' + data[iteration].dataset_code);
					if (common.checkValid(parsedResponse) && common.checkValid(parsedResponse.dataset_data) && common.checkValid(parsedResponse.dataset_data.data[0])) {
						parsedResponse.dataset_data.database_code = data[iteration].database_code;
						parsedResponse.dataset_data.dataset_code = data[iteration].dataset_code;
						parsedResponse.dataset_data.name = data[iteration].name;
						parsedResponse.dataset_data.last_download = parsedResponse.dataset_data.data[0][0];
						parsedResponse.dataset_data.prices = [];
						parseToObject(parsedResponse.dataset_data.data, parsedResponse.dataset_data.prices);
						DatasetContents.update({ 'dataset_code': parsedResponse.dataset_data.dataset_code }, parsedResponse.dataset_data, { upsert: true }, function (error) {
							if (error)
								cleanJSON(error);
						});
						if (iteration + 1 < data.length)
							downloadAndStoreDatasetContents(data, ++iteration);
					}
					else {
						downloadAndStoreDatasetContents(data, iteration);
					}
				}
				else {
					downloadAndStoreDatasetContents(data, iteration);
				}
			}
		});
	} catch (error) {
		console.log('Failed to download data for ' + data[iteration].dataset_code + ' due to the following exception');
		cleanJSON(error);
		cleanJSON('Reattempting to download data for ' + data[iteration].dataset_code);
		downloadAndStoreDatasetContents(data, iteration);
	}
}

function updateDatasetContents(data, iteration, continueFlag) {
	if (iteration >= data.length)
		return;
	var database_code = data[iteration].database_code;
	var dataset_code = data[iteration].dataset_code;
	var last_download = data[iteration].last_download;
	var nextDate = new Date(last_download);//Important to pass this parameter as timezone of newly created date is IST and not UTC, else issue will come when app is run between 00:00 and 05:30 hours.
	nextDate.setDate(last_download.getUTCDate() + 1);
	var startDateInMySqlFormat = nextDate.toISOString().slice(0, 10).replace('T', ' ');
	var url = 'https://www.quandl.com/api/v3/datasets/' + database_code + '/' + dataset_code + '/data.json?auth_token=' + quandlAuthToken + '&api_version=2015-04-09&start_date=' + startDateInMySqlFormat;
	try {
		request.get({ 'url': url, 'timeout': 8000 }, function (error, response) {
			if (error) {
				cleanJSON(error);
				updateDatasetContents(data, iteration, continueFlag);
			}
			else {
				console.log('(' + (iteration + 1) + '/' + data.length + ') ' + 'Downloading data for ' + dataset_code);
				if (common.checkValid(response) && common.checkValid(response.body)) {
					var parsedResponse = JSON.parse(response.body);
					if (common.checkValid(parsedResponse) && common.checkValid(parsedResponse.dataset_data) && common.checkValid(parsedResponse.dataset_data.data[0])) {
						parsedResponse.dataset_data.prices = [];
						parseToObject(parsedResponse.dataset_data.data, parsedResponse.dataset_data.prices);
						DatasetContents.update({ 'dataset_code': dataset_code }, {
							$addToSet: {
								'prices': {
									$each: parsedResponse.dataset_data.prices
								}
							},
							$set: {
								last_download: parsedResponse.dataset_data.data[0][0]
							}
						}, function (error) {
							if (error)
								cleanJSON(error);
							else {
								DatasetContents.update({ 'dataset_code': dataset_code }, { $push: { prices: { $each: [], $sort: -1 } } }, function (error) {
									if (error)
										cleanJSON(error);
								});
							}
						});
					}
				}
				else {
					throw URIError;
				}
				if (common.checkValid(continueFlag)) {
					updateDatasetContents(data, iteration + maxSimultaneousUpdates, true);
					for (var i = 0; i < maxSimultaneousUpdates - 1; i++)
						updateDatasetContents(data, ++iteration);
				}
			}
		});
	} catch (error) {
		console.log('Failed to download data for ' + data[iteration].dataset_code + ' due to the following exception');
		cleanJSON(error);
		cleanJSON('Reattempting to download data for ' + data[iteration].dataset_code);
		updateDatasetContents(data, iteration, continueFlag);
	}
}

function setPseudoToday(beginDay, callback) {
	DatasetContents.find({ 'dataset_code': 'CNX_NIFTY' }, { 'prices': { $slice: [beginDay, 1] } }, function (error, data) {
		if (error)
			cleanJSON(error);
		else {
			pseudoToday = data[0].prices[0].Date;
			callback();
		}
	});
}

function queryStocks(minSharePrice, beginDay, endDay, minGainPercent, maxGainPercent, minVolume, minTurnover, recoveringFromInDays, recoveryPercent, callback) {
	var iteration,
		selectedScrips = [],
		i;
	DatasetContents.find({}, { 'prices': { $slice: [beginDay, endDay] } }, function (error, filteredData) {
		if (error)
			cleanJSON(error);
		else {
			for (iteration = 0; iteration < filteredData.length; iteration++) {
				var dataset_code = filteredData[iteration].dataset_code;
				var name = filteredData[iteration].name;
				if (!common.checkValid(filteredData[iteration].prices[0]) || filteredData[iteration].column_names.length === 7)
					continue;
				if (common.checkValid(recoveringFromInDays))
					if (!common.checkValid(filteredData[iteration].prices[recoveringFromInDays]))
						continue;
				var lowestClosePrice = filteredData[iteration].prices[0].Close;
				var lowestClosePriceOnDate = filteredData[iteration].prices[0].Date;
				var highestClosePrice = filteredData[iteration].prices[0].Close;
				var highestClosePriceOnDate = filteredData[iteration].prices[0].Date;
				for (i = 1; i < filteredData[iteration].prices.length; i++) {
					if (lowestClosePrice > filteredData[iteration].prices[i].Close) {
						lowestClosePrice = filteredData[iteration].prices[i].Close;
						lowestClosePriceOnDate = filteredData[iteration].prices[i].Date;
					}
					if (highestClosePrice < filteredData[iteration].prices[i].Close) {
						highestClosePrice = filteredData[iteration].prices[i].Close;
						highestClosePriceOnDate = filteredData[iteration].prices[i].Date;
					}
				}
				var minPrice = lowestClosePrice + (lowestClosePrice * minGainPercent);
				var maxPrice = lowestClosePrice + (lowestClosePrice * maxGainPercent);
				if (filteredData[iteration].prices[0].Close > minSharePrice && (filteredData[iteration].prices[0].Close > minPrice) && (filteredData[iteration].prices[0].Close < maxPrice) && filteredData[iteration].prices[0]['Total Trade Quantity'] > minVolume && filteredData[iteration].prices[0]['Turnover (Lacs)'] > minTurnover) {
					var obj = {
						'dataset_code': dataset_code,
						'name': name,
						'lowestClosePrice': lowestClosePrice,
						'lowestClosePriceOnDate': lowestClosePriceOnDate,
						'highestClosePrice': highestClosePrice,
						'highestClosePriceOnDate': highestClosePriceOnDate,
						'lastestClosePrice': filteredData[iteration].prices[0].Close,
						'lastestDate': filteredData[iteration].prices[0].Date,
						'latestVolume': filteredData[iteration].prices[0]['Total Trade Quantity'],
						'latestTurnover': filteredData[iteration].prices[0]['Turnover (Lacs)']
					};
					var uptrend = true;
					if (common.checkValid(recoveringFromInDays)) {
						lowestClosePrice = filteredData[iteration].prices[0].Close;
						for (i = 1; i < recoveringFromInDays; i++) {
							if (lowestClosePrice > filteredData[iteration].prices[i].Close) {
								lowestClosePrice = filteredData[iteration].prices[i].Close;
								lowestClosePriceOnDate = filteredData[iteration].prices[i].Date;
							}
						}
						minPrice = lowestClosePrice + (lowestClosePrice * recoveryPercent);
						if ((filteredData[iteration].prices[0].Close < minPrice))
							uptrend = false;
					}
					else
						uptrend = true;
					if (obj.lastestDate >= pseudoToday && uptrend)
						selectedScrips.push(obj);
				}
			}
			callback(selectedScrips);
		}
	});
}

function transformData(data, iteration) {
	if (iteration >= data.length)
		return;
	var dataset_code = data[iteration].dataset_code;
	DatasetContents.find({ 'dataset_code': dataset_code }, function (error, filteredData) {
		if (error)
			cleanJSON(error);
		else {
			var obj = [];
			var i;
			if (filteredData[0].column_names[4] === 'Last')
				for (i = 0; i < filteredData[0].data.length; i++) {
					obj[i] = {
						'Date': filteredData[0].data[i][0],
						'Open': filteredData[0].data[i][1],
						'High': filteredData[0].data[i][2],
						'Low': filteredData[0].data[i][3],
						'Last': filteredData[0].data[i][4],
						'Close': filteredData[0].data[i][5],
						'Total Trade Quantity': filteredData[0].data[i][6],
						'Turnover (Lacs)': filteredData[0].data[i][7]
					};
				}
			else
				for (i = 0; i < filteredData[0].data.length; i++) {
					obj[i] = {
						'Date': filteredData[0].data[i][0],
						'Open': filteredData[0].data[i][1],
						'High': filteredData[0].data[i][2],
						'Low': filteredData[0].data[i][3],
						'Close': filteredData[0].data[i][4],
						'Shares Traded': filteredData[0].data[i][5],
						'Turnover (Rs. Cr)': filteredData[0].data[i][6]
					};
				}
			DatasetContents.update({ 'dataset_code': dataset_code }, {
				$addToSet: {
					'prices': {
						'$each': obj
					}
				}
			}, function (error, result) {
				if (error)
					cleanJSON(error);
				else {
					cleanJSON(iteration + 1);
					transformData(data, ++iteration);
				}
			});
		}
	});
}

router.get('/', function (req, res) {
	res.json({ 'message': 'Welcome to /api' });
	debug('testing');
});

router.put('/downloadDatasets', function (req, res) {
	downloadDatasets([], 1, function (error, builtDatasets) {
		if (error)
			cleanJSON(error);
		else {
			builtDatasets.forEach(function (dataset) {
				NSEDatasets.update({ dataset_code: dataset.dataset_code }, dataset, { upsert: true }, function (error) {
					if (error)
						cleanJSON(error);
				});
			});
			console.log('Download completed, db writes running asynchronously');
		}
	});
	res.json({ 'message': 'Started dataset retrieval' });
});

router.put('/removeDelistedDatasets', function (req, res) {
	var oldestDateAsDate = new Date(oldestDate);
	NSEDatasets.find({}, { _id: 0, 'database_code': 1, 'dataset_code': 1, 'newest_available_date': 1 }, function (error, data) {
		for (var i = 0; i < data.length; i++){
			if (data[i].newest_available_date < oldestDateAsDate){
				DelistedDatasets.update({ dataset_code: data[i].dataset_code }, data[i], { upsert: true }, function (error) {
					if (error)
						cleanJSON(error);
				});
			}
		}
	});
	res.json({'message': 'Delisting in progress'});
});

router.put('/downloadDataForDatasetsWithoutData', function (req, res) {
	NSEDatasets.find({}, { _id: 0, 'database_code': 1, 'dataset_code': 1 }, { 'sort': { 'dataset_code': 1 } }, function (error, data1) {
		if (error)
			cleanJSON(error);
		else {
			DatasetContents.find({}, { _id: 0, 'database_code': 1, 'dataset_code': 1 }, function (error, data2) {
				for (var i = 0; i < data1.length; i++)
					data1[i] = data1[i].dataset_code;
				for (i = 0; i < data2.length; i++)
					data2[i] = data2[i].dataset_code;
				var diff = common.arrayDiff.call(data1, data2);
				DelistedDatasets.find({}, { _id: 0, 'dataset_code': 1 }, function (error, data3) {
					for (var i = 0; i < data3.length; i++)
						data3[i] = data3[i].dataset_code;
					var notInDelisted = common.arrayDiff.call(diff, data3);
					NSEDatasets.find({ dataset_code: { $in: notInDelisted } }, { _id: 0, 'database_code': 1, 'dataset_code': 1, 'name': 1 }, { 'sort': { 'dataset_code': 1 } }, function (error, data) {
						if (common.checkValid(data) && common.checkValid(data[0]))
							downloadAndStoreDatasetContents(data, 0);
						// cleanJSON(data); Use this print statement to insert delisted stocks into 'delisteddatasets' database
					});
				});
			});
		}
	});
	res.json({ 'message': 'Started retrieval of data for each of the dataset in database' });
});

router.post('/updateDataForExistingDatasets', function (req, res) {
	var skip = Number(req.query.skip) || 0;
	DatasetContents.find({}, { _id: 0, 'database_code': 1, 'dataset_code': 1, 'last_download': 1 }, { 'sort': { 'dataset_code': 1 }, 'skip': skip }, function (error, data) {
		updateDatasetContents(data, 0, true);
	});
	res.json({ 'message': 'Started updating data for all existing datasets' });
});

router.get('/queryStocks/', function (req, res) {
	var minSharePrice = Number(req.query.minSharePrice) || 20;
	var beginDay = Number(req.query.beginDay) || 0;
	var endDay = Number(req.query.endDay) || 15;
	var minGainPercent = (Number(req.query.minGainPercent) / 100) || 0.2;
	var maxGainPercent = (Number(req.query.maxGainPercent) / 100) || 1000;
	var minVolume = Number(req.query.minVolume) || 0;
	var minTurnover = Number(req.query.minTurnover) || 0;
	var recoveringFromInDays = (common.checkValid(req.query.recoveringFromInDays) ? Number(req.query.recoveringFromInDays) : null);
	var recoveryPercent = (Number(req.query.recoveryPercent)) / 100 || 0;
	if (minGainPercent >= maxGainPercent)
		res.json({ 'message': 'minGainPercent should be lesser than maxGainPercent' });
	else
		setPseudoToday(beginDay, function () {
			queryStocks(minSharePrice, beginDay, endDay, minGainPercent, maxGainPercent, minVolume, minTurnover, recoveringFromInDays, recoveryPercent, function (selectedScrips) {
				res.json(selectedScrips);
			});
		});
});

router.param('name', function (req, res, next, name) {
	req.nameMod = name.toUpperCase();
	next();
});

router.post('/demo1/:name', function (req, res) {
	res.status(202).json({
		'message': 'Okay',
		'body': req.body,
		'query': req.query,
		'cookie': req.cookie,
		'params': req.params,
		'nameMod': req.nameMod,
		'headers': req.headers
	});
});

router.route('/demo2').
	put(function (req, res) {
		res.json({ 'PUT': 'Create user' });
	}).
	get(function (req, res) {
		res.json({ 'GET': 'Send form' });
	}).
	post(function (req, res) {
		res.json({ 'POST': 'Update user' });
	}).
	delete(function (req, res) {
		res.json({ 'DELETE': 'Delete user' });
	});

router.get('/test', function (req, res) {
	DatasetContents.find({}, { '_id': 0, 'dataset_code': 1 }, function (error, data) {
		if (error)
			cleanJSON(error);
		else {
			transformData(data, 0);
			res.json({
				'message': 'Test'
			});
		}
	});
});

module.exports = router;