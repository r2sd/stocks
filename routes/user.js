var express = require('express');
var router = express.Router();
var common = require('../src/common.js');
var jwt = require('jsonwebtoken');
var Users = require('../model/schema').UserModel;

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

var config = require('../config.js');
var fbOptions = config.fbOptions;
var serverSecret = config.serverSecret;

function cleanJSON(data) {
	console.log(common.cleanJSON(data));
}

passport.use(new FacebookStrategy(fbOptions, function (accessToken, refreshToken, profile, done) {
	Users.findOrCreate({ 'facebook.id': profile.id }, function (error, result) {
		if (error)
			done(error, result);
		else if (result) {
			result.name = profile.displayName;
			result.facebook.id = profile.id;
			result.facebook.access_token = accessToken;
			result.save(function (error, document) {
				if (error)
					cleanJSON(error);
				done(error, document);
			});
		}
	});
}));

router.get('/auth/facebook', passport.authenticate('facebook', {
	session: false,
	scope: ['email', 'public_profile', 'user_friends']
}));

router.get('/auth/facebook/callback', passport.authenticate('facebook', { session: false, failureRedirect: '/user' }), function (req, res) {
	res.redirect('/user/me');
});

router.route('/').
	put(function (req, res) {
		res.json({ 'PUT': 'Create user' });
	}).
	get(function (req, res) {
		res.send('<a href="/user/auth/facebook">Log in</a>');
	}).
	post(function (req, res) {
		res.json({ 'POST': 'Update user' });
	}).
	delete(function (req, res) {
		res.json({ 'DELETE': 'Delete user' });
	});

router.get('/me', function (req, res) {
	req.token = jwt.sign({
		'_id': req._id,
		'account_type': req.account_type,
		'name': req.name
	}, serverSecret, {
		expiresIn: '7d',
		issuer: 'shridharshan@stocks'
	});
	res.json({'token':req.token});
});

module.exports = router;
