//Use this module to interact personally with a client
//Use the exported io object to emit information to all connected clients
//See index.js for an example on how to communicate with all connected clients

//Also both express and io listen to the same port!
//io object can shared among different routes
//Both of these have been achieved using the stackoverflow answer below
//http://stackoverflow.com/a/28288406

var io = require('socket.io')();

io.on('connection', function (socket) {
	socket.on('connected', function (data) {
		console.log(data);
		exports.socket = socket;
	});
});

exports.io = io;