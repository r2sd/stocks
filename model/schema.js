var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	name: String,
	account_type: String,
	facebook: {
		id: { type: String, unique: true },
		access_token: String
	}
});

UserSchema.statics.findOrCreate = function (filters, cb) {
	var User = this;
	this.find(filters, function (error, results) {
		if (results.length == 0) {
			var newUser = new User();
			newUser.facebook.id = filters.facebookId;
			newUser.account_type = 'facebook';
			newUser.save(function (err, doc) {
				cb(err, doc);
			});
		} else {
			cb(error, results[0]);
		}
	});
};

exports.UserModel = mongoose.model('User', UserSchema);

var NSEdatasetSchema = new Schema({
	id: Number,
	dataset_code: {
		type: String,
		index: { unique: true, dropDups: true }
	},
	database_code: String,
	name: String,
	description: String,
	refreshed_at: Date,
	newest_available_date: Date,
	oldest_available_date: Date,
	column_names: Array,
	frequency: String,
	type: String,
	premium: Boolean,
	last_update: Date,//Refers to last update on Quandl servers at the time of retrieving the dataset
	database_id: Number
});

exports.NSEDatasetModel = mongoose.model('nsedataset', NSEdatasetSchema);

var DatasetContentSchema = new Schema({
	dataset_code: {
		type: String,
		index: { unique: true, dropDups: true }
	},
	database_code: String,
	name: String,
	limit: Number,
	transform: String,
	column_index: Number,
	column_names: Array,
	start_date: Date,//Obsolete
	end_date: Date,//Obsolete
	last_download: Date,//Refers to last update on local database
	description: String,
	frequency: String,
	prices: [
		{
			'_id': String,
			'Date':Date,
			'Open':Number,
			'High':Number,
			'Low': Number,
			'Last':Number,
			'Close':Number,
			'Shares Traded': Number,
			'Total Trade Quantity':Number,
			'Turnover (Rs. Cr)': Number,//only  for exchange indices
			'Turnover (Lacs)':Number//only for exchange actual scrips
		}
	],
	collapse: String,
	order: String
});

exports.DatasetContentModel = mongoose.model('datasetcontent', DatasetContentSchema);

var DelistedDatasetSchema = new Schema({
	dataset_code: {
		type: String,
		index: { unique: true, dropDups: true }
	},
	database_code: String,
	name: String
});

exports.DelistedDatasetModel = mongoose.model('delisteddataset', DelistedDatasetSchema);